{-# LANGUAGE OverloadedStrings #-}

module MintChipMain (main) where

import qualified SDL
import qualified SDL.Font as Font

import Linear (V2(..))

import MintChip.Loader.Loader
import MintChip.Game
import MintChip.Types

main :: IO ()
main = do
  SDL.initialize [SDL.InitVideo]
  Font.initialize
  -- get a window, renderer, and other resources
  win <- SDL.createWindow "Mint Chip" wcfg
  ren <- SDL.createRenderer win (-1) SDL.defaultRenderer
  assets <- loadAssets ren
  -- run the game, providing the config data
  runGame GameConfig
    { _ren = ren
    , _win = win
    , _assets = assets
    , _windowWidth = ww
    , _windowHeight = wh
    , _finishLine = 8000 }
  -- clean up the horrible mess we just made
  SDL.destroyWindow win
  Font.quit
  SDL.quit
    where ww = 800
          wh = 400
          wcfg = SDL.defaultWindow
            { SDL.windowInitialSize =
                V2 (fromIntegral ww) (fromIntegral wh) }
  
