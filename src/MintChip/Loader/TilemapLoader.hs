module MintChip.Loader.TilemapLoader (loadTilemapData) where

import qualified Data.Aeson.Tiled as Tiled
import qualified Data.Vector as V

import Control.Lens
import Data.Maybe (fromJust)
import Linear (V2(..), V4(..))

import MintChip.Types

-- | Gets all the important data from a tilemap
loadTilemapData :: FilePath -> IO (V.Vector Tile, V.Vector Enemy, V.Vector Token)
loadTilemapData path = do
  tileMap <- Tiled.loadTiledmap "assets/test-map.json"
  let tileMap' = case tileMap of
        Left err -> error $ "Could not load tilemap -- " ++ err
        Right tmap -> tmap
  return (readTerrain tileMap', readEnemies tileMap', readTokens tileMap')

-- | Reads a tile map into a list of tiles.
readTerrain :: Tiled.Tiledmap -> V.Vector Tile
readTerrain tmap = layerToGameGridWith gidToTile . fromJust $ getLayerData tmap 0
  where gidToTile i (Tiled.GlobalId gid) =
          let tileSize = Tiled.tiledmapTilewidth tmap -- assume tiles are square
              tilemapWidth = Tiled.tiledmapWidth tmap
              pos = pure tileSize * V2 (i `mod` tilemapWidth) (i `div` tilemapWidth)
          in case gid of
               1 -> Just $ Tile { _tilePos = pos, _tileType = TileType'Stalagmites }
               2 -> Just $ Tile { _tilePos = pos, _tileType = TileType'Block }
               3 -> Just $ Tile { _tilePos = pos, _tileType = TileType'Stalactites }
               _ -> Nothing

-- | Reads a tile map into a list of enemies.
readEnemies :: Tiled.Tiledmap -> V.Vector Enemy
readEnemies tmap = layerToGameGridWith gidToEnemy . fromJust $ getLayerData tmap 1
  where gidToEnemy i (Tiled.GlobalId gid) =
          let tileSize = Tiled.tiledmapTilewidth tmap -- assume tiles are square
              tilemapWidth = Tiled.tiledmapWidth tmap
              pos = pure tileSize * V2 (i `mod` tilemapWidth) (i `div` tilemapWidth) 
          in case gid of
               6 -> Just $ Enemy { _enemyPos = pos
                                 , _enemyType = EnemyType'LaserTurret
                                 , _enemyHp = 50
                                 , _enemyAmmoType = AmmoType'Laser Alignment'Bad
                                 , _enemyIsDead = False
                                 , _enemyFireDelay = 0 }
               _ -> Nothing

-- | Reads a tile map into a list of tokens.
readTokens :: Tiled.Tiledmap -> V.Vector Token
readTokens tmap = layerToGameGridWith gidToToken . fromJust $ getLayerData tmap 2
  where gidToToken i (Tiled.GlobalId gid) =
          let tileSize = Tiled.tiledmapTilewidth tmap -- assume tiles are square
              tilemapWidth = Tiled.tiledmapWidth tmap
              pos = pure tileSize * V2 (i `mod` tilemapWidth) (i `div` tilemapWidth) 
          in case gid of
               4 -> Just $ Token { _tokenPos = pos
                                 , _tokenType = TokenType'IceCream
                                 , _isCollected = False }
               5 -> Just $ Token { _tokenPos = pos
                                 , _tokenType = TokenType'Heart
                                 , _isCollected = False }
               _ -> Nothing

-- | Tries to Extract the given layer from the provided tilemap
getLayerData :: Tiled.Tiledmap -> Int -> Maybe (V.Vector Tiled.GlobalId)
getLayerData tmap layerNum
  | layerNum >= 0 = Tiled.layerData . (V.! layerNum) . Tiled.tiledmapLayers $ tmap
  | otherwise = Nothing

-- | Converts a Tiled layer (with coordinates) into a Vector of @a@s provided a converter function
layerToGameGridWith :: (Int -> Tiled.GlobalId -> Maybe a) -> V.Vector Tiled.GlobalId -> V.Vector a
layerToGameGridWith gidToFoo = V.mapMaybe (uncurry gidToFoo) . V.indexed
