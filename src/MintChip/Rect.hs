module MintChip.Rect (Rect(..)) where

import Control.Lens
import Linear (V2(..))

import MintChip.Types

-- | A typeclass for things with a rectangular shape.
class Rect a where
  -- | Coordinates of the @Rect@'s upper-left corner.
  location :: a -> V2 Int
  -- | The width and height of the @Rect@
  dimensions :: a -> V2 Int
  
  x :: a -> Int
  x = (^._1) . location

  y :: a -> Int
  y = (^._2) . location

  width :: a -> Int
  width = (^._1) . dimensions

  height :: a -> Int
  height = (^._2) . dimensions

instance Rect Ammo where
  location = (^.ammoPos)
  dimensions ammo = 
    case ammo^.ammoType of
      AmmoType'Laser _ -> V2 25 2
      _ -> undefined

instance Rect Enemy where
  location = (^.enemyPos)
  dimensions = const $ V2 16 16

instance Rect Player where
  location = (^.playerPos)
  dimensions = const $ V2 32 32

instance Rect Tile where
  location = (^.tilePos)
  dimensions = const $ V2 16 16

instance Rect Token where
  location = (^.tokenPos)
  dimensions = const $ V2 16 16
