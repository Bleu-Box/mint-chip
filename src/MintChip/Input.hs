module MintChip.Input (controlIsPressed, pumpEvents) where

import qualified SDL
import qualified SDL.Input.Keyboard as Keys

import Control.Monad.IO.Class

import MintChip.Types

pumpEvents :: MonadIO m => m ()
pumpEvents = SDL.pumpEvents

-- | Check if the keyboard control for the given action is pressed.
controlIsPressed :: MonadIO m => ControlAction -> m Bool
controlIsPressed ctrl = ($ controlToScancode ctrl) <$> SDL.getKeyboardState 

-- | Convert a ControlAction to the corresponding Scancode.
controlToScancode :: ControlAction -> Keys.Scancode
controlToScancode sc =
  case sc of
    ControlAction'Up -> Keys.ScancodeW
    ControlAction'Down -> Keys.ScancodeS
    ControlAction'FireLaser -> Keys.ScancodeF
    ControlAction'Enter -> Keys.ScancodeX
    ControlAction'Quit -> Keys.ScancodeEscape
    ControlAction'GoToMenu -> Keys.ScancodeM
