module MintChip.Entities.Token (updateToken) where

import Control.Lens
import Control.Monad.Reader
import Control.Monad.State
import Linear (V2(..))

import MintChip.Collide
import MintChip.Rect
import MintChip.Renderer
import MintChip.Types

updateToken :: Token -> MintChip Token
updateToken token = gets (^.player) >>= \player ->
  return $ isCollected .~ (player `isColliding` token) $ token
