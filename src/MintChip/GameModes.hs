{-# LANGUAGE OverloadedStrings #-}

module MintChip.GameModes (mainMenu, playGame) where

import qualified Data.Vector as V
import qualified SDL
import qualified SDL.Font as Font

import Control.Lens
import Control.Monad.Reader
import Control.Monad.State
import Data.List (intercalate, sortOn)
import Data.Ord (Down(..))
import Linear.V2 (V2(..))
import System.Directory (removeFile, renameFile)

import MintChip.Entities.Ammo
import MintChip.Loader.Loader (loadGameState)
import MintChip.Collide
import MintChip.Entities.Enemy
import MintChip.Input
import MintChip.Entities.Player
import MintChip.Rect
import MintChip.Renderer
import MintChip.Entities.Token
import MintChip.Types

mainMenu :: MintChip ()
mainMenu = do
  menuTex <- asks (^.assets.menuTitle)
  -- Render the title
  renderTexture menuTex (V2 50 50)
  -- Render the high scores
  renderHighScores
  -- Transition the mode if Enter key is pressed.
  enterPressed <- liftIO $ controlIsPressed ControlAction'Enter
  when enterPressed $ mode .= GameMode'Play

playGame :: MintChip ()
playGame = do
  player' <- gets (^.player)
  finishLine <- asks (^.finishLine)
  tileList <- gets (^.terrain)
  ammoList <- gets (^.ammo)
  enemyList <- gets (^.enemies)
  tokenList <- gets (^.tokens)
  -- render everything
  V.forM_ tileList renderWrtPlayer
  V.forM_ ammoList renderWrtPlayer
  V.forM_ enemyList renderWrtPlayer
  V.forM_ tokenList renderWrtPlayer
  renderWrtPlayer player'
  renderHUD
  -- If the player is dead, display the death message and wait for the
  -- player to press a key before transitioning to the menu and resetting the player.
  -- If the user hasn't supplied keyboard input yet, make sure to skip the rest of the
  -- game loop until we get that input.
  if player'^.playerIsDead then
    do deathTitle <- asks (^.assets.deathTitle)
       menuPressed <- liftIO $ controlIsPressed ControlAction'GoToMenu
       -- display death message
       renderTexture deathTitle (V2 250 250)
       -- re-load game state
       when menuPressed $
         resetGameState >> liftIO (recordScore $ player'^.score)
    else if x player' > finishLine then
           do winTitle <- asks (^.assets.winTitle)
              menuPressed <- liftIO $ controlIsPressed ControlAction'GoToMenu
              -- display success message
              renderTexture winTitle (V2 250 250)
              -- re-load game state
              when menuPressed $ do
                -- give player a score bonus
                liftIO (recordScore $ (player'^.score) + 100)
                resetGameState
         else do w <- asks (^.windowWidth)
                 h <- asks (^.windowHeight)
                 -- Update everything:
                 -- update the player
                 -- (Make sure to update player before ammo so as to avoid this one
                 -- insidious bug where ammo hits the player twice before dying. I'm
                 -- not sure why that happens.)
                 updatePlayer
                 -- update ammo
                 ammoList <- gets (^.ammo) 
                 newAmmo <- mapM updateAmmo ammoList
                 ammo .= newAmmo
                 -- update the enemies
                 enemyList <- gets (^.enemies)
                 newEnemies <- V.mapM updateEnemy enemyList
                 enemies .= newEnemies
                 -- update the tokens
                 tokenList <- gets (^.tokens)
                 newTokens <- V.mapM updateToken tokenList
                 tokens .= newTokens
                 -- Clean up all the dead/irrelevant stuff.
                 ammo %= V.filter (\ammo -> not (ammo^.ammoIsDead)
                                    && (x ammo + width ammo) > x player'
                                    && x ammo < (w + x player'))
                 enemies %= V.filter (\enemy -> not (enemy^.enemyIsDead)
                                       && (x enemy + width enemy) > x player')
                 tokens %= V.filter (\token -> not (token^.isCollected)
                                      && (x token + width token) > x player')
                 terrain %= V.filter (\tile -> (x tile + width tile) > x player')

-- | Render a game object using a camera focused around the player.
-- The player's y-position isn't considered in the offset so that the game
-- only scrolls from the side.
renderWrtPlayer :: (Renderable a b) => a -> MintChip ()
renderWrtPlayer a = gets (^.player) >>= \player ->
  let playerCam = Cam $ playerPos._2 .~ 0 $ player
  in render playerCam a

renderHUD :: MintChip ()
renderHUD = do
  health <- gets (^.player.playerHp)
  score <- gets (^.player.score)
  healthTex <- asks (^.assets.healthTitle)
  scoreTex <- asks (^.assets.scoreTitle)
  w <- asks (^.windowWidth)
  h <- asks (^.windowHeight)
  -- render labels & corresponding stats
  renderTexture healthTex (V2 (w - 150) 1)
  renderInt health (V2 (w - 50) 1)
  renderTexture scoreTex (V2 (w - 150) 20)
  renderInt score (V2 (w - 50) 20)

renderHighScores :: MintChip ()
renderHighScores = do
  w <- asks (^.windowWidth)
  h <- asks (^.windowHeight)
  highScoresTitle <- asks (^.assets.highScoresTitle)
  scoreString <- liftIO $ readFile "highscores.txt"
  let scores = map read $ lines scoreString
  -- render the title for the score list
  renderTexture highScoresTitle (V2 (w `div` 4) 100)
  -- render each score at a different y-position
  forM_ (zip [1..] scores) $ \(i, score) ->
    renderInt score (V2 (w `div` 4) (115 + i * 15))

resetGameState :: MintChip ()
resetGameState = do
  cfg <- ask
  freshGameState <- liftIO $ loadGameState cfg
  put freshGameState
  
recordScore :: Int -> IO ()
recordScore newScore = do
  scoreString <- readFile "highscores.txt"
  let scoreInts = map read $ lines scoreString
      newScores = take 5 . sortOn Down $ newScore:scoreInts
      newScoreString = intercalate "\n" $ map show newScores
  writeFile "highscores_new.txt" newScoreString
  removeFile "highscores.txt"
  renameFile "highscores_new.txt" "highscores.txt"
