module MintChip.Game (runGame) where

import qualified Data.Vector as V
import Linear (V2(..))
import qualified SDL

import Control.Concurrent (threadDelay)
import Control.Lens
import Control.Monad (unless)
import Control.Monad.Reader
import Control.Monad.State
import Data.Time
import Data.Time.Clock

import MintChip.Loader.Loader
import MintChip.GameModes
import MintChip.Input
import MintChip.Renderer
import MintChip.Types

runGame :: GameConfig -> IO ((), GameState)
runGame cfg = loadGameState cfg >>= \initialState -> 
  runStateT (runReaderT (runMintChip mainLoop) cfg) initialState

mainLoop :: MintChip ()
mainLoop = do
  mode <- gets (^.mode)
  ren <- asks (^.ren)
  -- update events
  pumpEvents
  -- update sprites and screen
  updateSprites  
  SDL.clear ren
  -- choose the scene function to use based on the game mode
  case mode of
    GameMode'Menu -> mainMenu
    GameMode'Play -> do
      start <- liftIO getCurrentTime
      playGame
      stop <- liftIO getCurrentTime
      -- get the time updating game took
      let elapsedTime = 0.000001 * (realToFrac $ diffUTCTime stop start :: Double)
          delayTime = 1000 - round elapsedTime
      -- wait however long is needed to maintain a top speed limit
      when (delayTime > 0) $ liftIO (threadDelay delayTime)
  SDL.present ren
  -- 'break' out of loop if a Quit action was recieved
  quitPressed <- liftIO $ controlIsPressed ControlAction'Quit
  unless quitPressed mainLoop
